class AddProjectsToPillars < ActiveRecord::Migration[5.2]
  def change
    add_reference :pillars, :project, foreign_key: true
  end
end
