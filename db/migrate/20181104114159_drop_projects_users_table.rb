class DropProjectsUsersTable < ActiveRecord::Migration[5.2]
  def up
  	drop_table :projects_users
  end

  def down
  	raise ActiveRecord::irreversibleMigration
  end
end
