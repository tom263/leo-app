class DropPillarsProjectsTable < ActiveRecord::Migration[5.2]
  def up
  	drop_table :pillars_projects
  end

  def down
  	raise ActiveRecord::IrreversibleMigration
  end
end
