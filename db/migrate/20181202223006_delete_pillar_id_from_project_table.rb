class DeletePillarIdFromProjectTable < ActiveRecord::Migration[5.2]
  def change
    remove_column :projects, :pillar_id
  end
end
