class AddUserToPillars < ActiveRecord::Migration[5.2]
  def change
    add_reference :pillars, :user, foreign_key: true
  end
end
