class AddPillarToProjects < ActiveRecord::Migration[5.2]
  def change
    add_reference :projects, :pillar, foreign_key: true
  end
end
