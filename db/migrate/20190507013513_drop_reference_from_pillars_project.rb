class DropReferenceFromPillarsProject < ActiveRecord::Migration[5.2]
  def change
    remove_column :pillars, :project_id
  end
end
