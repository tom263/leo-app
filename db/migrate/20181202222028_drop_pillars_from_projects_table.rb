class DropPillarsFromProjectsTable < ActiveRecord::Migration[5.2]

  def change
    remove_index :projects, :pillar_id
  end
end
