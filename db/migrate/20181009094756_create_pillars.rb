class CreatePillars < ActiveRecord::Migration[5.2]
  def change
    create_table :pillars do |t|
      t.string :title
      t.text :description
      t.text :vision
      t.string :goal_1
      t.string :goal_2
      t.string :goal_3
      t.string :project_1
      t.string :project_2
      t.string :project_3
      t.string :skill_1
      t.string :skill_2
      t.string :skill_3
      t.string :event_1
      t.string :event_2
      t.string :event_3

      t.timestamps
    end
  end
end
