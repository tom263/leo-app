class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :title
      t.string :summary
      t.string :why
      t.string :problem_description
      t.string :goal_statement
      t.text :project_details
      t.text :resources
      t.integer :budget
      t.text :goals_extended
      t.text :success_measures

      t.timestamps
    end
  end
end
