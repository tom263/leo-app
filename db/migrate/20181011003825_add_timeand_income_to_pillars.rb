class AddTimeandIncomeToPillars < ActiveRecord::Migration[5.2]
  def change
    add_column :pillars, :time, :string
    add_column :pillars, :income, :integer
  end
end
