class CreateJoinTablePillarsProjects < ActiveRecord::Migration[5.2]
  def change
    create_join_table :projects, :pillars do |t|
       t.index [:project_id, :pillar_id]
       t.index [:pillar_id, :project_id]
    end
  end
end
