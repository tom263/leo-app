require "administrate/base_dashboard"

class PillarDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    image_attachment: Field::HasOne,
    image_blob: Field::HasOne,
    projects: Field::HasMany,
    id: Field::Number,
    title: Field::String,
    description: Field::Text,
    vision: Field::Text,
    goal_1: Field::String,
    goal_2: Field::String,
    goal_3: Field::String,
    project_1: Field::String,
    project_2: Field::String,
    project_3: Field::String,
    skill_1: Field::String,
    skill_2: Field::String,
    skill_3: Field::String,
    event_1: Field::String,
    event_2: Field::String,
    event_3: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    Cover: Field::String,
    time: Field::String,
    income: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :user,
    :image_attachment,
    :image_blob,
    :projects,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :image_attachment,
    :image_blob,
    :projects,
    :id,
    :title,
    :description,
    :vision,
    :goal_1,
    :goal_2,
    :goal_3,
    :project_1,
    :project_2,
    :project_3,
    :skill_1,
    :skill_2,
    :skill_3,
    :event_1,
    :event_2,
    :event_3,
    :created_at,
    :updated_at,
    :Cover,
    :time,
    :income,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :image_attachment,
    :image_blob,
    :projects,
    :title,
    :description,
    :vision,
    :goal_1,
    :goal_2,
    :goal_3,
    :project_1,
    :project_2,
    :project_3,
    :skill_1,
    :skill_2,
    :skill_3,
    :event_1,
    :event_2,
    :event_3,
    :Cover,
    :time,
    :income,
  ].freeze

  # Overwrite this method to customize how pillars are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(pillar)
  #   "Pillar ##{pillar.id}"
  # end
end
