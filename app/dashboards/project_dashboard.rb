require "administrate/base_dashboard"

class ProjectDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    pillar: Field::BelongsTo,
    image_attachment: Field::HasOne,
    image_blob: Field::HasOne,
    id: Field::Number,
    title: Field::String,
    summary: Field::String,
    why: Field::String,
    problem_description: Field::String,
    goal_statement: Field::String,
    project_details: Field::Text,
    resources: Field::Text,
    budget: Field::Number,
    goals_extended: Field::Text,
    success_measures: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :pillar,
    :image_attachment,
    :image_blob,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :pillar,
    :image_attachment,
    :image_blob,
    :id,
    :title,
    :summary,
    :why,
    :problem_description,
    :goal_statement,
    :project_details,
    :resources,
    :budget,
    :goals_extended,
    :success_measures,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :pillar,
    :image_attachment,
    :image_blob,
    :title,
    :summary,
    :why,
    :problem_description,
    :goal_statement,
    :project_details,
    :resources,
    :budget,
    :goals_extended,
    :success_measures,
  ].freeze

  # Overwrite this method to customize how projects are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(project)
  #   "Project ##{project.id}"
  # end
end
