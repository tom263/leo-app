class ProjectsController < ApplicationController
	before_action :set_project, only: [:show, :update, :destory, :edit]
  before_action :authenticate_user!

  def index
  @projects = current_user.projects.all
  end

  def show

  end

  def new
  	@project = current_user.projects.build
  end

  def create
  	@project = current_user.projects.new(project_params)
    @project.image.attach(params[:project][:image])

  	respond_to do |format|
  		if @project.save
  			format.html { redirect_to projects_path}
  		else
  			format.html { render :new }
  		end
  	end
  end

  def edit
  end

  def update
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_path }
    end
  end

	private

	def set_project 
		@project = Project.find(params[:id])
	end

	def project_params
		params.require(:project).permit(:title, :summary, :why, :problem_description, :goal_statement, :project_details, :resources, :budget, :goals_extended, :success_measures, :image, :user_id)
	end
end
