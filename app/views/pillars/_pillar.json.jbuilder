json.extract! pillar, :id, :title, :description, :vision, :goal_1, :goal_2, :goal_3, :project_1, :project_2, :project_3, :skill_1, :skill_2, :skill_3, :event_1, :event_2, :event_3, :created_at, :updated_at
json.url pillar_url(pillar, format: :json)
