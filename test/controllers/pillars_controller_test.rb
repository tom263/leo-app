require 'test_helper'

class PillarsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pillar = pillars(:one)
  end

  test "should get index" do
    get pillars_url
    assert_response :success
  end

  test "should get new" do
    get new_pillar_url
    assert_response :success
  end

  test "should create pillar" do
    assert_difference('Pillar.count') do
      post pillars_url, params: { pillar: { description: @pillar.description, event_1: @pillar.event_1, event_2: @pillar.event_2, event_3: @pillar.event_3, goal_1: @pillar.goal_1, goal_2: @pillar.goal_2, goal_3: @pillar.goal_3, project_1: @pillar.project_1, project_2: @pillar.project_2, project_3: @pillar.project_3, skill_1: @pillar.skill_1, skill_2: @pillar.skill_2, skill_3: @pillar.skill_3, title: @pillar.title, vision: @pillar.vision } }
    end

    assert_redirected_to pillar_url(Pillar.last)
  end

  test "should show pillar" do
    get pillar_url(@pillar)
    assert_response :success
  end

  test "should get edit" do
    get edit_pillar_url(@pillar)
    assert_response :success
  end

  test "should update pillar" do
    patch pillar_url(@pillar), params: { pillar: { description: @pillar.description, event_1: @pillar.event_1, event_2: @pillar.event_2, event_3: @pillar.event_3, goal_1: @pillar.goal_1, goal_2: @pillar.goal_2, goal_3: @pillar.goal_3, project_1: @pillar.project_1, project_2: @pillar.project_2, project_3: @pillar.project_3, skill_1: @pillar.skill_1, skill_2: @pillar.skill_2, skill_3: @pillar.skill_3, title: @pillar.title, vision: @pillar.vision } }
    assert_redirected_to pillar_url(@pillar)
  end

  test "should destroy pillar" do
    assert_difference('Pillar.count', -1) do
      delete pillar_url(@pillar)
    end

    assert_redirected_to pillars_url
  end
end
