require "application_system_test_case"

class PillarsTest < ApplicationSystemTestCase
  setup do
    @pillar = pillars(:one)
  end

  test "visiting the index" do
    visit pillars_url
    assert_selector "h1", text: "Pillars"
  end

  test "creating a Pillar" do
    visit pillars_url
    click_on "New Pillar"

    fill_in "Description", with: @pillar.description
    fill_in "Event 1", with: @pillar.event_1
    fill_in "Event 2", with: @pillar.event_2
    fill_in "Event 3", with: @pillar.event_3
    fill_in "Goal 1", with: @pillar.goal_1
    fill_in "Goal 2", with: @pillar.goal_2
    fill_in "Goal 3", with: @pillar.goal_3
    fill_in "Project 1", with: @pillar.project_1
    fill_in "Project 2", with: @pillar.project_2
    fill_in "Project 3", with: @pillar.project_3
    fill_in "Skill 1", with: @pillar.skill_1
    fill_in "Skill 2", with: @pillar.skill_2
    fill_in "Skill 3", with: @pillar.skill_3
    fill_in "Title", with: @pillar.title
    fill_in "Vision", with: @pillar.vision
    click_on "Create Pillar"

    assert_text "Pillar was successfully created"
    click_on "Back"
  end

  test "updating a Pillar" do
    visit pillars_url
    click_on "Edit", match: :first

    fill_in "Description", with: @pillar.description
    fill_in "Event 1", with: @pillar.event_1
    fill_in "Event 2", with: @pillar.event_2
    fill_in "Event 3", with: @pillar.event_3
    fill_in "Goal 1", with: @pillar.goal_1
    fill_in "Goal 2", with: @pillar.goal_2
    fill_in "Goal 3", with: @pillar.goal_3
    fill_in "Project 1", with: @pillar.project_1
    fill_in "Project 2", with: @pillar.project_2
    fill_in "Project 3", with: @pillar.project_3
    fill_in "Skill 1", with: @pillar.skill_1
    fill_in "Skill 2", with: @pillar.skill_2
    fill_in "Skill 3", with: @pillar.skill_3
    fill_in "Title", with: @pillar.title
    fill_in "Vision", with: @pillar.vision
    click_on "Update Pillar"

    assert_text "Pillar was successfully updated"
    click_on "Back"
  end

  test "destroying a Pillar" do
    visit pillars_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pillar was successfully destroyed"
  end
end
