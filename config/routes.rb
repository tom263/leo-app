Rails.application.routes.draw do
  namespace :admin do
      resources :users
      resources :pillars
      resources :projects

      root to: "users#index"
    end

 devise_for :users

  authenticated :user do
	root "pillars#index", as: :authenticated_root
  end

  root "pages#home"
  get "story", to: "pages#story"

  resources :projects
  resources :pillars 


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
